
import express from "express";

export const router = express.Router();

export default { router };


router.get("/luz", (req, res) => {
  res.render("index", { titulo: "Acerca De" });
});

router.get("/pagos", (req, res) => {
  const params = {
    titulo: "Recibo",
    numero: req.query.numero,
    nombre: req.query.nombre,
    domicilio: req.query.domicilio,
    servicio: req.query.servicio,
    kwConsumidos: req.query.kwConsumidos,
    isPost: false,
  };
  res.render("pagos", params);
});

router.post("/pagos", (req, res) => {
  const precios = [1.08, 2.5, 3.0];

  const { numero, nombre, domicilio, servicio, kwConsumidos } = req.body;
  if(kwConsumidos <= 0){
    res.redirect('/pagos');
  }else{
    const precioKw = precios[servicio * 1];
    const tipoDeServicio = servicio == 0 ? 'Domestico' :  servicio == 1 ? 'Comercial' : 'Industrial';
    const subtotal = precioKw * kwConsumidos;
  
    // Calcular el descuento
    let descuento = 0;
    if (kwConsumidos <= 1000) {
      descuento = 0.1;
    } else if (kwConsumidos > 1000 && kwConsumidos <= 10000) {
      descuento = 0.2;
    } else {
      descuento = 0.5;
    }
    // Calcular el impuesto
    const impuesto = 0.16 * subtotal;
  
    // Aplicar el descuento al subtotal
    const descuentoAplicado = subtotal * descuento;
    const subtotalConDescuento = subtotal - descuentoAplicado;
    
    // Calcular el total a pagar
    const total = subtotalConDescuento + impuesto;
  
    // Actualizar el objeto 'resultado'
    const params = {
      titulo: "Recibo",
      numero,
      nombre,
      domicilio,
      servicio: tipoDeServicio,
      kwConsumidos,
      precioKw,
      subtotal,
      descuento: descuentoAplicado, // Actualizado para reflejar el descuento aplicado
      subtotalConDescuento,
      impuesto,
      total,
      isPost: true,
    };
    console.log(params);
    res.render("pagos", params);
  }
});

router.get("/contactos", (req, res) => {
    res.render("contactos", { titulo: "Ontiveros Govea" });
});

